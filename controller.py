# FLASK_APP=controller.py flask run  --host=0.0.0.0 --port=5000
from flask import Flask, request
import mxnet as mx

from gluoncv import model_zoo, data
import mxnet.gluon.data.vision.transforms as transforms
import mxnet as mx
import numpy as np
import threading
import importlib
import gluoncv
import time
from threading import Thread
from PIL import Image

app = Flask(__name__)
s = 0
net = model_zoo.get_model('ssd_512_resnet50_v1_voc', pretrained=True)

net = model_zoo.get_model('ssd_512_resnet50_v1_voc', pretrained=True)
to_tensor = transforms.Compose([transforms.ToTensor(), transforms.Normalize(0, 1)])



@app.route('/posts', methods=['GET', 'POST'])
def hello_world():
    global s
    global net

    if request.method == 'POST':
        print("oli post")
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        print("on file")
        file = request.files['file']
        img = Image.open(file)
        print(type(img))
        pix = np.array(img)
        print(pix.shape, "pix.shape")
    else:
        print("oli get")
    #tmp_thread = Thread(target=sub_worker, args=['count'])
    #tmp_thread.start()
    #global net
    #global to_tensor

    time1a = time.time()
    to_tensor = transforms.Compose([transforms.ToTensor(), transforms.Normalize(0, 1)])

    x, img = data.transforms.presets.ssd.load_test('/Users/timonikkila/projects/enhanced_art/mlserver/testimage.jpg', short=512)
    time1 = time.time()
    print(time1 - time1a, "testi2")

    print("testi3")
    img_t = to_tensor(mx.nd.array(img))
    print("testi4")
    img_t = img_t.expand_dims(axis=0)
    print("testi5")

    class_IDs, scores, bounding_boxs = net(img_t)

    time2 = time.time()
    print(time2-time1, "time2")
    scores_np = scores[0].asnumpy()
    bounding_np = bounding_boxs[0].asnumpy()
    indices = np.where(scores_np > 0.5)[0]

    #print(indices)
    winnig_s = scores_np[indices]* 100

    winning_b = bounding_np[indices]
    winning_b = np.vstack((winning_b[:,2]-winning_b[:,0], winning_b[:,3]-winning_b[:,1]))
    winning_b = np.swapaxes(winning_b, 0,1)

    combined_f = np.hstack((winnig_s,winning_b)).astype(int)
    print(combined_f)

    time2 = time.time()
    print(time2-time1a, "time3")
    s += 1

    #index_add_counter += 1
    return 'Hello, World4! ' + str(s)

if __name__ == "__main__":
    # app.run(host='192.168.43.150', port="5000")
    app.run(host= '0.0.0.0')
